package testSiteCURA;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

class ConnexionTest2 {

	@Test
	public void lancerBankProject() throws InterruptedException {
		WebDriverManager.edgedriver().setup();
		ChromeDriver driver = new ChromeDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.get("http://demo.guru99.com/V1/index.php");
		Thread.sleep(10000);
		driver.switchTo().frame("gdpr-consent-notice") ;
		

		driver.findElement(By.id("save")).click();
		driver.switchTo().parentFrame() ;
		
		Assertions.assertTrue(driver.findElement(By.name("btnLogin")).isDisplayed());
		driver.findElement(By.name("uid")).sendKeys("mngr378088");
		driver.findElement(By.name("password")).sendKeys("gYnetYg");

		driver.findElement(By.name("btnLogin")).click();
		//Assertions.assertTrue(driver.findElement(By.linkText("Log out")).isEnabled());
		//driver.findElement(By.linkText("New Customer")).click();
		//Assertions.assertEquals("Welcome To Manager's Page of GTPL Bank",driver.findElement(By.className("heading3")).getText());
		//Thread.sleep(10000);
		//Assertions.assertEquals("dsdsds",driver.findElement(By.name("name")).getAttribute("value"));
		driver.quit();
		
	}
	
	
	@Test
	public void lancerBankProject2() throws InterruptedException {
		
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://demo.guru99.com/V1/index.php");
		Thread.sleep(5000);
		
		driver.quit();
		
	}

	
	
	@Test
	public void lancerDolibarrTiers() throws InterruptedException {
		
		ChromeDriver driver = new ChromeDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.get("http://dolibarr.testlogiciel.io/");

		driver.findElement(By.id("username")).sendKeys("jsmith");
		driver.findElement(By.name("password")).sendKeys("Dawan2021");
		driver.findElement(By.className("button")).click();
		driver.findElement(By.id("mainmenua_companies")).click();
		driver.findElement(By.linkText("Nouveau tiers")).click();
		driver.findElement(By.xpath("//input[@id='name']")).sendKeys("toto");
		new Select(driver.findElement(By.id("customerprospect"))).selectByVisibleText("Prospect");
		Thread.sleep(3000);
		driver.findElement(By.cssSelector("input.button")).click();
		Thread.sleep(3000);

		
		
		driver.quit();
		
	}
	
}
