package dolibarrObject;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class  LoginPage {

	WebDriver driver;
	@FindBy(id="username")
	WebElement textLogin;
	
	@FindBy(id="password")
	WebElement textPassword;
	
	@FindBy(id="login-submit-wrapper")
	WebElement logginButton;
	
	public LoginPage(WebDriver driver) {
		this.driver=driver;
	
//		textLogin=driver.findElement(By.id("username"));
//		textPassword=driver.findElement(By.id("password"));
//		logginButton=driver.findElement(By.id("button"));
	}
	
	public void saisirLogin(String user) {
		this.textLogin.clear();
		this.textLogin.sendKeys(user);
		
	}
	
	public void saisirPassword(String password) {
		this.textPassword.clear();
		this.textPassword.sendKeys(password);
		
	}

	public void cliquerConnexion() {
		this.logginButton.click();
	
	}
	
}
