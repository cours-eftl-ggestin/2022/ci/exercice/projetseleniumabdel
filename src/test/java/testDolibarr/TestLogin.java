package testDolibarr;

import static org.junit.jupiter.api.Assertions.*;

import java.time.Duration;



import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import dolibarrObject.LoginPage;
import io.github.bonigarcia.wdm.WebDriverManager;



class TestLogin {

	ChromeDriver driver;
	
	@BeforeEach
	public void setup(){
		WebDriverManager.edgedriver().setup();
		driver= new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.get("http://dolibarr.testlogiciel.io/");
	}
	
	@AfterEach
	public void tearDown() {
		driver.quit();
	}
	
	@Test
	public void testConnexionAvecIdentifiantsValide() {
		
		LoginPage loginPage= new LoginPage(driver);
		
		PageFactory.initElements(driver, loginPage);
		loginPage.saisirLogin("jsmith");
		loginPage.saisirPassword("Dawan2021");
		loginPage.cliquerConnexion();
		
//		AccuielPage accueilPage= new AccuielPage(driver);
//		AssertionError.as
//		accueilPage
	}

}
