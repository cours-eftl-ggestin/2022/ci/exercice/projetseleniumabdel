package orangeSuite;

import static org.junit.jupiter.api.Assertions.*;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Utility {

protected static  WebDriver browser;

	
	public void enterText(By by,String string) {

		isElementPresent(by);
		// find the input text box
		WebElement element = browser.findElement(by);
		
		element.clear();

		// set the user name in input text box
		element.sendKeys(string);

	}
	
	public void selectValueInListBox(By byElement, String value) {
		
		WebElement element = browser.findElement(byElement);
		Select select = new Select(element);
		
		select.selectByValue(value);
	}

	
	
	public void isElementPresent(By by) {

		
			assertTrue(browser.findElement(by).isEnabled());
			assertTrue(browser.findElement(by).isDisplayed());

	}
	
	public void clickElement(By by)  {

		WebDriverWait wait=new WebDriverWait(browser,Duration.ofSeconds(4));
		wait.until(ExpectedConditions.visibilityOf(browser.findElement(by)));
		
		isElementPresent(by);
		
		

		//get the actual link text
		WebElement element=browser.findElement(by); 

		// click the link
		element.click();

	}
	
	public void verifyTitle(String expectedTitle) {
		//get the title of the page
		String actualTitle = browser.getTitle();

		// verify title
		assertEquals(actualTitle, expectedTitle);
	}

}
