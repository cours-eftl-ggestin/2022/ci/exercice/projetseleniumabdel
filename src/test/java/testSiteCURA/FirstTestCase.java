package testSiteCURA;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class FirstTestCase {
	
	
	public static void test() throws Exception {

		// Create a new instance of the Firefox driver

		WebDriverManager.edgedriver().setup();
		ChromeDriver driver = new ChromeDriver();
		  //System.setProperty("webdriver.gecko.driver", "C://Users/Moi/Documents/geckodriver.exe");

		//Launch the Online Store Website

		driver.get("http://www.all4test.fr");

		// Print a Log In message to the screen

		System.out.println("Successfully opened the website www.Store.Demoqa.com");

		//Wait for 5 Sec

		Thread.sleep(5000);

		// Close the driver

		driver.quit();

		}

}
