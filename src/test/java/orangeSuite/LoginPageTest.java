package orangeSuite;




import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import orangeSuiteObjects.Driver;
import orangeSuiteObjects.LoginPageOrangeSuite;



public class LoginPageTest {


		public static  WebDriver browser;
		
		
		@BeforeAll
		public static void preConditionLogin() {
		
		lancerLeNavigateur("Chrome", "http://orangehrm.testlogiciel.io/symfony/web");				
		
		}
		

		
		@AfterAll
		public static void tearDown()  {
			
			browser.quit();
			System.out.println("quitter");
		
		}
		
		
		@Test
		public  void TestSeLoguer()  {
			
			seLoguer("admin","Selenium&2018");
			
		}
		
		
		public static void lancerLeNavigateur(String navigateur, String adress) {
			
			
			Driver driver= new Driver(browser);
			driver.ouvrirNavigateur(navigateur,  adress);
			
			browser=driver.getBrowser();
		
		}
	
	public static  void seLoguer(String id, String Mdp)  {

		LoginPageOrangeSuite loginPage = new LoginPageOrangeSuite(browser);
		PageFactory.initElements(browser, loginPage);
		loginPage.saisirLogin(id);
		loginPage.saisirPassword(Mdp);
		loginPage.cliquerConnexion();
		
		
	}


}
