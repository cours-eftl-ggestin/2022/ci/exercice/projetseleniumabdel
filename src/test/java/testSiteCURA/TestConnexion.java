//package testSiteCURA;
//
//
//import static org.junit.jupiter.api.Assertions.*;
//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.openqa.selenium.By;
//import org.openqa.selenium.JavascriptExecutor;
//import org.openqa.selenium.NoSuchElementException;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.chrome.ChromeOptions;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.firefox.FirefoxOptions;
//import org.openqa.selenium.firefox.FirefoxProfile;
//import org.openqa.selenium.remote.DesiredCapabilities;
//
//
//import java.time.Duration;
//
//
//public class TestConnexion {
//
//
//	private WebDriver driver;
//	private ChromeOptions browserOptions;
//	private FirefoxProfile profile;
//	private StringBuffer verificationErrors = new StringBuffer();
//	JavascriptExecutor js;
//
//
//	@BeforeEach
//	public  void testOuvrir() {
//	//	System.setProperty("webdriver.gecko.driver", "C://Users/Karim/Documents/geckodriver.exe");
//
//		this.browserOptions= new ChromeOptions();
//
//		this.browserOptions.setAcceptInsecureCerts(true);
//
//		DesiredCapabilities dc = new DesiredCapabilities();
//		dc.setCapability("marionette", true);
//		browserOptions.merge(dc);
//
//		this.browserOptions.setCapability("marionette", true);
//
//		this.profile= new FirefoxProfile();
//		this.browserOptions.setProfile(profile);
//
//		this.driver = new ChromeDriver(browserOptions);
//		js = (JavascriptExecutor) driver;
//
//		this.driver.get("https://katalon-demo-cura.herokuapp.com/");
//		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
//
//		System.out.println("ouvrir la page cura");
//	}
//
//	@AfterEach
//	public void tearDown()  {
//		System.out.println("quitter");
//		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
//		this.driver.quit();
//		String verificationErrorString = verificationErrors.toString();
//		if (!"".equals(verificationErrorString)) {
//			fail(verificationErrorString);
//		}
//	}
//
//	@Test
//	public void testConnexion() 
//	{
//
//		// verify title of index page
//		verifyTitle("CURA Healthcare Service");
//		verifyHeaderMessage(By.tagName("h3"),"We Care About Your Health");
//
//		// go to profile page: click on Make Appointment button
//		System.out.println("ouvrir la page de connexion");
//		clickElement(By.id("btn-make-appointment"),"Make Appointment");
//
//		//driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(50));
//
//		//verify title of connexion page
//		verifyTitle("CURA Healthcare Service");
//
//		//verify header of connexion page
//		verifyHeaderMessage(By.tagName("h2"),"Login");
//
//
//		//enter user name as John Doe
//		enterText(By.id("txt-username"),"John Doe");
//
//		//enter user password 
//		enterText(By.id("txt-password"),"blabla");
//
//
//
//		//go to Appointment page: click on Login button
//		clickElement(By.id("btn-login"),"Login"); 
//
//		//afficher l'alerte
//		alertPresent("text-danger");
//		alertTextPresent("text-danger","Login failed! Please ensure the username and password are valid.");
//		enterText(By.id("txt-username"),"John Doe");
//		enterText(By.id("txt-password"),"ThisIsNotAPassword");
//
//		//go to Appointment page: click on Login button
//		System.out.println("ouvrir la page de reservation");
//		clickElement(By.id("btn-login"),"Login"); 
//
//
//		//verify title and header of Appointment page 
//		verifyTitle("CURA Healthcare Service");
//		verifyHeaderMessage(By.tagName("h2"),"Make Appointment");
//
//		//driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(50));
//	}
//
//	private void verifyTitle(String expectedTitle) {
//		//get the title of the page
//		String actualTitle = this.driver.getTitle();
//
//		// verify title
//		assertEquals(actualTitle, expectedTitle);
//	}
//
//
//	private void verifyHeaderMessage(By by, String expectedHeaderMessage) {
//
//		//			ebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(3));
//		//			
//		//			wait.until(ExpectedConditions.visibilityOfElementLocated(by));W
//
//		assertTrue(this.isElementPresent(by));
//
//		// find header element
//		WebElement element = driver.findElement(by);
//
//
//
//		String actualHeaderMessage = element.getText();
//
//		// verify header text
//		assertEquals(actualHeaderMessage, expectedHeaderMessage);
//	}
//
//	private void enterText(By by,String userName) {
//
//		assertTrue(this.isElementPresent(by));
//		// find the input text box
//		WebElement element = driver.findElement(by);
//
//		// set the user name in input text box
//		element.sendKeys(userName);
//
//		// submit form
//		//element.submit();
//	}
//
//
//
//	private void clickElement(By by, String linkText)  {
//
//		assertTrue(this.isElementPresent(by));
//
//
//		//get the actual link text
//		WebElement element=driver.findElement(by); 
//		String actualLinkText = element.getText(); 
//
//		//verify link text with expected like text
//		assertEquals(actualLinkText,linkText);
//
//		// click the link
//		element.click();
//
//	}
//
//
//
//
//	private boolean isElementPresent(By by) {
//		try {
//			this.driver.findElement(by);
//			return true;
//		} catch (NoSuchElementException e) {
//			System.out.println(e);
//			return false;
//
//
//		}
//	}
//
//	private void  alertPresent(String className) {
//
//
//		assertTrue( this.isElementPresent(By.className(className)));
//
//;
//	};
//
//	private void alertTextPresent(String className,String messageAlert) {
//		assertEquals(messageAlert,this.driver.findElements(By.className(className)).get(0).getText());
//
//	}
//
//
//
//}
//
//



