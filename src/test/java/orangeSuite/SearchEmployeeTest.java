package orangeSuite;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import orangeSuiteObjects.EmployeeSearchPageOrangeSuite;

public class SearchEmployeeTest {

	//public static   ChromeDriver browser;
		public static WebDriver browser;
		
		
		@BeforeAll
		public static void preConditionEmployeeResearch() throws InterruptedException {
		
			AddEmployeePersonalDetailTest.preConditionAddEmployeeDetailInfo();;
			AddEmployeePersonalDetailTest.addEmployeeDetailInfo();;
			browser=AddEmployeePersonalDetailTest.browser;
		}
		
		
		
		@AfterAll
		public static void tearDown()  {
			
			browser.quit();
			System.out.println("quitter");
		
		}
		
		@Test
		public static  void searchEmployee() throws InterruptedException  {
			
			EmployeeSearchPageOrangeSuite employeeList = new EmployeeSearchPageOrangeSuite(browser);
			PageFactory.initElements(browser, employeeList);
			
			employeeList.cliquerMenuPIM();
			employeeList.serchEmployee(AddEmployeePersonalDetailTest.employeeId);
			employeeList.clicEmployeeResult(AddEmployeePersonalDetailTest.employeeId);
			
			
		
		}



}
