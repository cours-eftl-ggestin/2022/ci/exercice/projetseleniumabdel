package orangeSuiteObjects;

import static org.junit.jupiter.api.Assertions.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class IndexOrangeList{
	
	WebDriver driver;
	
	public IndexOrangeList(WebDriver driver) {
		this.driver=driver;;
	}
	
	@FindBy(id="menu_pim_viewPimModule")
	WebElement menuPIM;

	@FindBy(id="welcome")
	WebElement welcome;
	
	@FindBy(linkText="Déconnexion")
	WebElement deconnexion;	


	
	public void welcomeLinkClic() {

		this.welcome.click();
		
	
	}
	
	public void deconnexionLinkClic() {

		this.deconnexion.click();
		
	
	}

	
	
	public void cliquerMenuPIM() {

		this.menuPIM.click();
	
	}
	
	public void verifyTitle(String expectedTitle) {
		//get the title of the page
		String actualTitle = this.driver.getTitle();

		// verify title
		assertEquals(actualTitle, expectedTitle);
	}
	
	
	
	
	

}
