package orangeSuiteObjects;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;


public class EditPersonalDetailsPageOrangeList  extends IndexOrangeList {
	
	WebDriver driver;
	
	@FindBy(id="personal_txtLicenNo")
	WebElement txtLicenNo;
	
	@FindBy(id="personal_optGender_1")
	WebElement optGender;
	
	@FindBy(id="personal_cmbMarital")
	WebElement personalCmbMarital;	
	
	@FindBy(id="personal_DOB")
	WebElement personalDOB;
	
	@FindBy(id="btnSave")
	WebElement saveButton;

	
	public EditPersonalDetailsPageOrangeList(WebDriver driver) {
		super(driver);
		this.driver=driver;
	}
	
	public void clicSaveButton() {
		
		this.saveButton.click();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(5));
	}
	
	public void enterLicenceNum(String txtLicenNo) {
		this.txtLicenNo.clear();
		this.txtLicenNo.sendKeys(txtLicenNo);
	}
	
	public void selectMaritalStatut(String statut){
		Select select = new Select(this.personalCmbMarital);
		select.selectByValue(statut);
		
	}
	
	public void selectMisterGender(){
		personalCmbMarital.click();
	}
	
	public void enterPersonnelDOM(String personalDOB) {
		this.personalDOB.clear();
		this.personalDOB.sendKeys(personalDOB);		
	}
	

}
