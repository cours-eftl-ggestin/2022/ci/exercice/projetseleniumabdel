package orangeSuite;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import orangeSuiteObjects.ViewEmployeePageOrange;

public class ViewEmployeeTest {

	//public static   ChromeDriver browser;
		public   String employeeId;
		public static WebDriver browser;
		
		
		@BeforeAll
		public static void preConditionViewEmployeeInfo() throws InterruptedException {
		
			SearchEmployeeTest.preConditionEmployeeResearch();;
			SearchEmployeeTest.searchEmployee();
			browser=SearchEmployeeTest.browser;
		}
		
		
		
		@AfterAll
		public static void tearDown()  {
			
			browser.quit();
			System.out.println("quitter");
		
		}
		
		@Test
		public  void testViewEmployee() throws InterruptedException  {
			
			
			ViewEmployeePageOrange employeePage = new ViewEmployeePageOrange(browser);
			PageFactory.initElements(browser, employeePage);
			employeePage.verifyFirstName("Damme");
			employeePage.verifyLasteName("Jean-Claude");
			

		
		}



}
