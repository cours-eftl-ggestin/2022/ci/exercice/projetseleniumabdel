package orangeSuiteObjects;

import static org.junit.jupiter.api.Assertions.*;

import java.time.Duration;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EmployeeSearchPageOrangeSuite extends IndexOrangeList {
	
	WebDriver driver;
	

	@FindBy(id="empsearch_id")
	WebElement searchEmployee;
	
	@FindBy(id="searchBtn")
	WebElement searchButton;	
	
	@FindBy(id="resultTable")
	WebElement tableResult;
	


	
	public EmployeeSearchPageOrangeSuite(WebDriver driver)  {
		super(driver);
		this.driver=driver;
		
	}

	public void serchEmployee( String searchEmployeeId) {
		//enterText(By.id("empsearch_id"),employeeId);
		this.searchEmployee.clear();
		this.searchEmployee.sendKeys(searchEmployeeId);
		this.searchButton.click();
		verfiyPresenceEmployee( searchEmployeeId);
		
	}
	
	
	
	public void clicEmployeeResult(String employeeId) {
		WebDriverWait wait=new WebDriverWait(driver,Duration.ofSeconds(5));
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.linkText(employeeId))));
		driver.findElement(By.linkText(employeeId)).click();;
		
	}
	
	private void verfiyPresenceEmployee(String employeeId) {
		assertEquals(tableResult.
				findElements(By.tagName("tr")).get(1).
				findElements(By.tagName("td")).get(1).
				getText(), employeeId);
	}

}
