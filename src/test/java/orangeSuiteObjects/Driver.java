package orangeSuiteObjects;

import java.time.Duration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;

import io.github.bonigarcia.wdm.WebDriverManager;


public class Driver {

	
	private  WebDriver browser;
	
	public Driver(WebDriver driver) {
		this.browser=driver;
	}
	
	
	public WebDriver getBrowser() {
		return browser;
	}


	public void ouvrirNavigateur(String navigateur, String PageAdress) {
		
		switch (navigateur) {
		
		case "Chrome":
			WebDriverManager.chromedriver().setup();
			browser = new ChromeDriver();
			browser.manage().deleteAllCookies();
			browser.manage().window().maximize();
		break;
		
		
		case "Firefox":
			WebDriverManager.firefoxdriver().setup();
			browser = new FirefoxDriver();
			browser.manage().deleteAllCookies();
			browser.manage().window().maximize();
		break;
		
		case "IE":
			WebDriverManager.iedriver().setup();
			browser = new InternetExplorerDriver();
			browser.manage().deleteAllCookies();
			browser.manage().window().maximize();
		
		case "Opera":
			WebDriverManager.operadriver().setup();
			browser = new OperaDriver();
			browser.manage().deleteAllCookies();
			browser.manage().window().maximize();
		break;
		
		case "Edge":
			WebDriverManager.edgedriver().setup();
			browser = new EdgeDriver();
			browser.manage().deleteAllCookies();
			browser.manage().window().maximize();
		break;
		}
		
		browser.get(PageAdress);	
		System.out.println("ouvrir la page orangehrm");
		
		browser.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
		browser.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(5));
		
		
		
	}

}
