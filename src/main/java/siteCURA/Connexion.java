package siteCURA;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Connexion {


	public static FirefoxDriver  ouvrirPageCURA() throws Exception {

		System.setProperty("webdriver.gecko.driver", "C://Users/Karim/Documents/geckodriver.exe");
		FirefoxDriver driver = new FirefoxDriver();
		//System.setProperty("webdriver.chrome.driver", "C:/chromedriver.exe");
		driver.get("https://katalon-demo-cura.herokuapp.com/");
		//RunEnvironment.setWebDriver(driver);
		return driver;
	}
	
	public static void  quitterPageCURA(FirefoxDriver driver) throws Exception {
		driver.quit();
	}
	

	
	public static  void ouvrirPageConnexion(FirefoxDriver driver) throws Exception {
	driver.findElement(By.id("btn-make-appointment")).click();
	}
	

	public static void login(FirefoxDriver driver) throws Exception {
		WebElement user=driver.findElement(By.id("txt-username"));
		user.sendKeys("John Doe");
	}

	public static void password(FirefoxDriver driver)  throws Exception {
		WebElement pass = driver.findElement(By.id("txt-password"));
		pass.sendKeys("ThisIsNotAPassword");
	}

	public static void confirmer_connexion(FirefoxDriver driver) throws Exception {
		driver.findElement(By.id("btn-login")).click();

	}

}
