package testSiteCURA;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Gecko_Driver {
	
	public static void tearDown(WebDriver driver) {
	   driver.quit();
	}
	
	
	public static void main(String[] args) throws InterruptedException {

		//System.setProperty("webdriver.gecko.driver", "C://Users/Moi/Documents/geckodriver.exe");
		WebDriverManager.edgedriver().setup();
		WebDriver driver = new ChromeDriver();

		driver.get("http://www.google.fr");

		Thread.sleep(5000);

		tearDown(driver);
		
		
		}

		
	
	

}
