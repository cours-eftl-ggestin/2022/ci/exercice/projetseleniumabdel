package orangeSuiteObjects;

import static org.junit.jupiter.api.Assertions.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class ViewEmployeePageOrange extends IndexOrangeList{
	
	WebDriver driver;
	
	@FindBy(id="personal_txtEmpFirstName")
	WebElement firstName;
	
	@FindBy(id="personal_txtEmpLastName")
	WebElement lastName;
	


	
	public ViewEmployeePageOrange(WebDriver driver) {
		super(driver);
		this.driver=driver;
	}
	
	public void verifyFirstName(String firstName) {
			
		assertEquals(this.firstName.getAttribute("value"), firstName);
	}
	
	public void verifyLasteName(String lastName) {
		
		assertEquals(this.lastName.getAttribute("value"), lastName);
	}
	
	

}
