package orangeSuiteObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class LoginPageOrangeSuite  {

	WebDriver driver;
	
	@FindBy(id="txtUsername")
	WebElement textLogin;
	
	@FindBy(id="txtPassword")
	WebElement textPassword;
	
	@FindBy(id="btnLogin")
	WebElement logginButton;
	
	public LoginPageOrangeSuite(WebDriver driver) {
		this.driver=driver;
	}
	
	public void saisirLogin(String user) {
		this.textLogin.clear();
		this.textLogin.sendKeys(user);
		
	}
	
	public void saisirPassword(String password) {
		this.textPassword.clear();
		this.textPassword.sendKeys(password);
		
	}

	public void cliquerConnexion() {

		this.logginButton.click();
	
	}
	
	

}
