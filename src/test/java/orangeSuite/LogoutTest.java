package orangeSuite;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import orangeSuiteObjects.IndexOrangeList;


public class LogoutTest {

		public static WebDriver browser;
		
		
		@BeforeAll
		public static void preConditionLougout() throws InterruptedException {
		
			SearchEmployeeTest.preConditionEmployeeResearch();;
			SearchEmployeeTest.searchEmployee();
			browser=SearchEmployeeTest.browser;
		}
		
		
		
		@AfterAll
		public static void tearDown()  {
			
			browser.quit();
			System.out.println("quitter");
		
		}
		
		@Test
		public  void test() throws InterruptedException  {
			
			
			IndexOrangeList indexPage=new IndexOrangeList(browser);
			PageFactory.initElements(browser, indexPage);
			indexPage.welcomeLinkClic();
			indexPage.deconnexionLinkClic();
		
		}



}
