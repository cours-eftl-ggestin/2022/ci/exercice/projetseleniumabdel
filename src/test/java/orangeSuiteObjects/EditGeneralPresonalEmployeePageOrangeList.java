package orangeSuiteObjects;

import java.time.Duration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EditGeneralPresonalEmployeePageOrangeList extends IndexOrangeList {
	
	WebDriver driver;
	
	@FindBy(id="firstName")
	WebElement name;
	
	@FindBy(id="middleName")
	WebElement secondName;
	
	@FindBy(id="lastName")
	WebElement prenom;	
	
	@FindBy(id="employeeId")
	WebElement employeeIdentifiant;
	
	@FindBy(id="btnAdd")
	WebElement addButton;
	
	@FindBy(id="btnSave")
	WebElement saveButton;	

	public EditGeneralPresonalEmployeePageOrangeList(WebDriver driver) {
		super(driver);
		this.driver=driver;;
	}
	
	public void saisirFirstName(String name) {
		this.name.clear();
		this.name.sendKeys(name);
		
	}
	
	public void saisirMiddleName(String secondName) {
		this.secondName.clear();
		this.secondName.sendKeys(secondName);
		
	}
	
	public void saisirLastName(String lastName) {
		this.prenom.clear();
		this.prenom.sendKeys(lastName);
		
	}
	
	public String recupererEmployeeId() {
		return this.employeeIdentifiant.getAttribute("value");
	}

	public void cliquerAjouter() {
		//driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
		//driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(5));
		//WebDriverWait wait=new WebDriverWait(driver,Duration.ofSeconds(5));
		//wait.until(ExpectedConditions.visibilityOf(saveButton));

		this.addButton.click();
		//driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
		//driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(5));
	
	}
	
	public void cliquerSauver() {
		//WebDriverWait wait=new WebDriverWait(driver,Duration.ofSeconds(5));
		//wait.until(ExpectedConditions.visibilityOf(saveButton));
		this.saveButton.click();
		//driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
		//driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(5));
	
	}



}
