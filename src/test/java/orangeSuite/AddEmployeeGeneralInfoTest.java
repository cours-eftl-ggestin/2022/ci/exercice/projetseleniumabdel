package orangeSuite;


import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
//import org.junit.jupiter.params.ParameterizedTest;
//import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import orangeSuiteObjects.*;


public class AddEmployeeGeneralInfoTest {
	

	 
		public static   String employeeId;
		public static WebDriver browser;
		
		
		@BeforeAll
		public static void preConditionAddEmployeeGeneralInfo() throws InterruptedException {
			LoginPageTest.preConditionLogin();;
			LoginPageTest.seLoguer("admin","Selenium&2018");
			browser=LoginPageTest.browser;
		}

		@AfterAll
		public static void tearDown()  {
			
			browser.quit();
			System.out.println("quitter");
		
		}
		
		
		public static void AddNamesEmployee(String name, String middleName, String lastName) throws InterruptedException  {
				
			
			//employee List 
			EditGeneralPresonalEmployeePageOrangeList addEmmployeePage =new EditGeneralPresonalEmployeePageOrangeList(browser);
			
			PageFactory.initElements(browser, addEmmployeePage);
			addEmmployeePage.verifyTitle("OrangeHRM");
			addEmmployeePage.cliquerMenuPIM();;
			
			
			addEmmployeePage.cliquerAjouter();
			addEmmployeePage.saisirFirstName(name);
			addEmmployeePage.saisirMiddleName(middleName);
			addEmmployeePage.saisirLastName(lastName);
			
			employeeId=addEmmployeePage.recupererEmployeeId();

			addEmmployeePage.cliquerSauver();
	
		}
		
		//@ParameterizedTest
	    
		@Test
		//@CsvFileSource(resources = "/orangeSuite/nom.csv", numLinesToSkip = 1)
		public void testAddEmployee() throws InterruptedException  {
			AddNamesEmployee("toto","titi","tata");
		}



}
