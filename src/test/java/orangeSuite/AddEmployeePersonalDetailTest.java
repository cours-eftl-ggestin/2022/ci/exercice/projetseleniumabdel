package orangeSuite;

import orangeSuiteObjects.EditPersonalDetailsPageOrangeList;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;


public class AddEmployeePersonalDetailTest {

	//public static   ChromeDriver browser;
		//public static   String employeeId;
		public static WebDriver browser;
		public static   String employeeId;
		
		
		@BeforeAll
		public static void preConditionAddEmployeeDetailInfo() throws InterruptedException {
		
			AddEmployeeGeneralInfoTest.preConditionAddEmployeeGeneralInfo();
			AddEmployeeGeneralInfoTest.AddNamesEmployee("Damme", "Van", "Jean-Claude");
			employeeId=AddEmployeeGeneralInfoTest.employeeId;
			browser=AddEmployeeGeneralInfoTest.browser;
		}
		
		
		
		@AfterAll
		public static void tearDown()  {
			
			browser.quit();
			System.out.println("quitter");
		
		}
		
	
		public static  void addEmployeeDetailInfo() throws InterruptedException  {
			
			
			
			EditPersonalDetailsPageOrangeList editDetail= new EditPersonalDetailsPageOrangeList(browser);
			PageFactory.initElements(browser, editDetail);
			editDetail.clicSaveButton();
			editDetail.enterLicenceNum("3465546454");
			editDetail.selectMisterGender();
			editDetail.selectMaritalStatut("Married");
			editDetail.enterPersonnelDOM("1978-07-05");
			editDetail.clicSaveButton();

			
		
		}
		
		@Test
		public  void testAddEmployeePersonnalDetail() throws InterruptedException   {
			
			
			
			addEmployeeDetailInfo();

			
		
		}



}
